"use strict";

// Esse arquivo contem os dados para os bancos

var Hotel = require("./hotelClass");
var Passagem = require("./passagemClass");
var contPass = 1;
var contHotel = 1;

// vetores com os dados
var fs = require("fs");


var HOTEISTEMP = JSON.parse(fs.readFileSync("arquivos/bancoHotelTmp.txt"));
var PASSTEMP = JSON.parse(fs.readFileSync("arquivos/bancoPassTmp.txt"));

var HOTEIS = JSON.parse(fs.readFileSync("arquivos/bancoHotel.txt"));
var PASSAGEM = JSON.parse(fs.readFileSync("arquivos/bancoPass.txt"));

function criaPassagem(id, origem, destino, limite) {
    var pass = new Passagem();
    pass.id = id;
    pass.origem = origem;
    pass.destino = destino;
    pass.limitePassageiros = limite;
    contPass++;
    return pass;
}
function criaHotel(id, nome, qtdQuartos, qtdPessoas) {
    var pass = new Hotel();
    pass.id = id;
    pass.nome = nome;
    pass.qtdQuartos = qtdQuartos;
    pass.qtdPessoas = qtdPessoas;
    contHotel++;
    return pass;
}

exports.HOTEIS = HOTEIS;
exports.PASSAGEM = PASSAGEM;
exports.HOTEISTEMP = HOTEISTEMP;
exports.PASSTEMP = PASSTEMP;