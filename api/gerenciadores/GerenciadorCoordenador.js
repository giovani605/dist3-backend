"use strict"
// esse arquivo vai ser o pincipal para controlar ..
// request eh um module externo
// estou utilizando ele pois o http padrao do node nao eh muito bom
var request = require('request');

// Recupera todos os hoteis cadastrados
function buscarTodosHotel(callback) {

    request.get("http://localhost:4000/hotel/", (error, response, body) => {
        // console.log('error:', error); // Print the error if one occurred
        // console.log('statusCode:', response && response.statusCode); // Print the response status code if a response was received
        console.log('body:', body); // Print the HTML for t
        body = JSON.parse(body);
        callback(body);
    });


}
// busca o hotel pelo id
function buscarHotelId(id, callback) {
    bancoHotel.buscarHotelId(id, (resultado) => {
        callback(resultado);
    });
}

// Reserva Hotel
function reservarHotel(dados, callback) {
    // Separa os dados do request
    console.log(dados);
    request({
        method: 'POST',
        headers: {
            "Content-Type": "application/json"
        },
        url: "http://localhost:4000/hotel/reservar",
        body: JSON.stringify(dados)
    }, (error, response, body) => {
        // console.log('error:', error); // Print the error if one occurred
        // console.log('statusCode:', response && response.statusCode); // Print the response status code if a response was received
        console.log('body:', body); // Print the HTML for t
        body = JSON.parse(body);
        callback(body["dados"]);
    });



}
// Recupera todas as passagens
function buscarTodasPassagem(callback) {
    request.get("http://localhost:4001/passagem/", (error, response, body) => {
        console.log('error:', error); // Print the error if one occurred
        console.log('statusCode:', response && response.statusCode); // Print the response status code if a response was received
        console.log('body:', body); // Print the HTML for t
        body = JSON.parse(body);
        callback(body);
    });

}
// busca a passagem pelo id
function buscarPassId(id, callback) {
    bancoPassagem.buscarPassId(id, (resultado) => {
        callback(resultado);
    });
}
// Reserva a passagem
function reservarPassagem(dados, callback) {
    // Separa os dados do request
    //  console.log("Reservar passagem: " + dataIda + " " + dataVolta + " " + origem + " " + destino);
    // Busca a passagem correspondente
    console.log(dados);
    request({
        method: 'POST',
        headers: {
            "Content-Type": "application/json"
        },
        url: "http://localhost:4001/passagem/reservar",
        body: JSON.stringify(dados)
    }, (error, response, body) => {
        // console.log('error:', error); // Print the error if one occurred
        // console.log('statusCode:', response && response.statusCode); // Print the response status code if a response was received
        console.log('body:', body); // Print the HTML for t
        body = JSON.parse(body);
        callback(body["dados"]);
    });

}


var moduleGerenciador = require("./gerenciadorTransacoes");
var gerenciadorTransacoes = new moduleGerenciador.GerenciadorTransacoes("logs/logCoordenador.txt");
// Reserva um pacote
function reservarPacote(dados, callback) {
    // cria a transacao
    var transacao = gerenciadorTransacoes.criarTransacao(dados);
    //console.log(dados);
    var contRespostas = 0;
    var mensagem = "";
    // funcao que lido com as respostas dos servidores hotel e passagem
    var lidarResposta = (tipo, resposta, msg) => {
        // colocar o voto no local corresponde
        // se os 2 votaram 
        // entra na fase de decisao

        mensagem += " " + msg;
        contRespostas++;
        if (tipo == "hotel") {
            transacao.votoHotel = resposta;
        } else {
            transacao.votoPassagem = resposta;
        }
        if (contRespostas == 2) {
            funcDecisao(transacao, mensagem);
        }

    }

    // funcao que eh chamada quando recebeu a resposta dos 2 servidores
    var funcDecisao = (transacao, mensagem) => {
        // testar se foi sucesso ou nao
        console.log("Decisao da transacao " + transacao.idTrans);
        console.log(transacao);
        if (transacao.votoHotel && transacao.votoPassagem) {
            console.log("SUCESSO NA  TRANSACAO");
            gerenciadorTransacoes.estadoEfetivacaoProvisoria(transacao);
            funcDecisaoSucesso(transacao);
            gerenciadorTransacoes.estadoEfetiva(transacao);
            callback("Sucesso na compra do pacote " + mensagem)

            return;
        } else {
            console.log("FRACASSO NA TRANSACAO");
            gerenciadorTransacoes.estadoFalha(transacao);
            funcDecicaoFalha(transacao);
            gerenciadorTransacoes.estadoAbortada(transacao);
            callback("Falha na compra do pacote " + mensagem);
            return;
        }
    }

    // sucesso na transacao
    var funcDecisaoSucesso = (transacao) => {
        request({
            method: 'POST',
            headers: {
                "Content-Type": "application/json"
            },
            url: "http://localhost:4000/hotel/efetivar/transacao/" + transacao.IdTrans,
        }, (error, response, body) => {
            // console.log('error:', error); // Print the error if one occurred
            // console.log('statusCode:', response && response.statusCode); // Print the response status code if a response was received
            // console.log('body:', body); // Print the HTML for t
            // body = JSON.parse(body);
            // console.log("resposta hotel efetivar");
            // console.log(body["dados"]);
        });

        request({
            method: 'POST',
            headers: {
                "Content-Type": "application/json"
            },
            url: "http://localhost:4001/passagem/efetivar/transacao/" + transacao.IdTrans,
        }, (error, response, body) => {
            // console.log('error:', error); // Print the error if one occurred
            // console.log('statusCode:', response && response.statusCode); // Print the response status code if a response was received
            // console.log('body:', body); // Print the HTML for t
            // body = JSON.parse(body);
            // console.log("resposta passagem Efetivar");
            // console.log(body["dados"]);
        });
        gerenciadorTransacoes.estadoEfetiva(transacao);
        return;
    }
    // falha
    var funcDecicaoFalha = (transacao) => {
        request({
            method: 'POST',
            headers: {
                "Content-Type": "application/json"
            },
            url: "http://localhost:4000/hotel/abortar/transacao/" + transacao.IdTrans,
        }, (error, response, body) => {
            // console.log('error:', error); // Print the error if one occurred
            // console.log('statusCode:', response && response.statusCode); // Print the response status code if a response was received
            // console.log('body:', body); // Print the HTML for t
            // body = JSON.parse(body);
            // console.log("resposta hotel efetivar");
            // console.log(body["dados"]);
        });

        request({
            method: 'POST',
            headers: {
                "Content-Type": "application/json"
            },
            url: "http://localhost:4001/passagem/abortar/transacao/" + transacao.IdTrans,
        }, (error, response, body) => {
            // console.log('error:', error); // Print the error if one occurred
            // console.log('statusCode:', response && response.statusCode); // Print the response status code if a response was received
            // console.log('body:', body); // Print the HTML for t
            // body = JSON.parse(body);
            // console.log("resposta passagem Efetivar");
            // console.log(body["dados"]);
        });
        gerenciadorTransacoes.estadoAbortada(transacao);
        return;
    }
    // Envia a transacao para os servidores responsaveis
    // primeira parte da transacao - fase abertura
    reservarHotelTrans(dados, transacao, lidarResposta);
    reservarPassTrans(dados, transacao, lidarResposta);
    
}

// Envia mensagem para o reservar hotel em uma transacao
function reservarHotelTrans(dados, transacao, callback) {
    // separa os dados para o request
    var dadosFinal = {
        "dataEntrada": dados.dataEntrada,
        "dataSaida": dados.dataSaida,
        "qtdQuartos": dados.qtdQuartos,
        "qtdPessoas": dados.qtdPessoas,
        "nome": dados.nome,
        "transacao": transacao
    };

    // executa o request
    // o 1 arg eh o header, url e metodo e o body
    request({
        method: 'POST',
        headers: {
            "Content-Type": "application/json"
        },
        url: "http://localhost:4000/hotel/reservar/transacao",
        body: JSON.stringify(dadosFinal)
    }, (error, response, body) => {
        // console.log('error:', error); // Print the error if one occurred
        // console.log('statusCode:', response && response.statusCode); // Print the response status code if a response was received
        // console.log('body:', body); // Print the HTML for t
        body = JSON.parse(body);
        console.log("resposta hotel");
        console.log(body["dados"]);
        callback("hotel", body["dados"]["voto"], body["dados"]["mensagem"]);
    });

}

// Envia mensagem para o reservar passagem em uma transacao
function reservarPassTrans(dados, transacao, callback) {

    var dadosFinal = {
        "dataIda": dados.dataEntrada,
        "dataVolta": dados.dataSaida,
        "origem": dados.origem,
        "qtdPessoas": dados.qtdPessoas,
        "destino": dados.destino,
        "tipo": dados.tipo,
        "transacao": transacao
    }

    request({
        method: 'POST',
        headers: {
            "Content-Type": "application/json"
        },
        url: "http://localhost:4001/passagem/reservar/transacao",
        body: JSON.stringify(dadosFinal)
    }, (error, response, body) => {
        // console.log('error:', error); // Print the error if one occurred
        // console.log('statusCode:', response && response.statusCode); // Print the response status code if a response was received
        //console.log('body:', body); // Print the HTML for t
        body = JSON.parse(body);
        console.log("resposta Pass");
        console.log(body["dados"]);
        callback("pass", body["dados"]["voto"], body["dados"]["mensagem"]);
    });


}

exports.reservarHotel = reservarHotel;
exports.reservarPacote = reservarPacote;
exports.reservarPassagem = reservarPassagem;
exports.buscarTodosHotel = buscarTodosHotel;
exports.buscarHotelId = buscarHotelId;
exports.buscarTodasPassagem = buscarTodasPassagem;
exports.buscarPassId = buscarPassId;



  // var dataIda = dados.dataEntrada;
    // var dataVolta = dados.dataSaida;
    // var dataEntrada = dados.dataEntrada;
    // var dataSaida = dados.dataSaida;
    // var qtdPessoas = Number(dados.qtdPessoas);
    // var qtdQuartos = Number(dados.qtdQuartos);
    // var origem = dados.origem;
    // var destino = dados.destino;
    // var nome = dados.nome;
    // var tipo = dados.tipo;

/*

// ativas
// var listaTransacoes = [];

// var contIdTrans = 0;

// function criarTransacao(dados) {
//     contIdTrans++;
//     return {
//         "votoHotel": false,
//         "votoPassagem": false,
//         "dados": dados,
//         "idTrans": contIdTrans,
//         "estado" : 1
//     }
// }
// 1- criado
// 
console.log("Reservar Pacote " + origem + " " + destino + " " + nome +
        " Pessoas:" + qtdPessoas + " QtdQuartos:" + qtdQuartos);

    // defino uma funcao para utilizar em caso de sucesso na reserva do pacote
    var funcSucesso = (hotelResultado, passResultado) => {

        passResultado.atualPassageiros += qtdPessoas;
        // Salvo dados do request na passagem
        passResultado.reservas.push({
            "dataIda": dataIda,
            "dataVolta": dataVolta,
            "qtdPessoas": qtdPessoas,
            "tipo": tipo
        })


        hotelResultado.qtdPessoasAtual += qtdPessoas;
        hotelResultado.qtdQuartosOcupados += qtdQuartos;
        // salvo dados de request no hotel
        hotelResultado.reservas.push({
            "dataEntrada": dataEntrada,
            "dataSaida": dataSaida,
            "qtdQuartos": qtdQuartos,
            "qtdPessoas": qtdPessoas
        })
        // Respondo ao cliente com uma mensagem de sucesso
        callback({
            "mensagem": "Pacote Reservado com sucesso",
            "hotel": hotelResultado,
            "passagem": passResultado
        });


    }
    // defino uma funcao para utilizar em caso de falha na reserva do pacote
    var funcFalha = (mensagem) => {
        // resposto ao cliente com uma mensagem de falha
        callback({ "mensagem": mensagem });
    }


    // consulta pasasgem
    bancoPassagem.buscarPassNome(origem, destino, (passResultado) => {
        if (!passResultado) {
            funcFalha("Nao encontrou passagem");
            return;
        }
        // tem espaço disponivel
        if (passResultado.limitePassageiros >= passResultado.atualPassageiros + qtdPessoas) {
            // consulta hotel
            bancoHotel.buscarHotelNome(nome, (hotelResultado) => {
                if (!hotelResultado) {
                    funcFalha("não encontrou o Hotel");
                    return;
                }
                // tem espaço disponivel
                if (hotelResultado.qtdPessoas >= hotelResultado.qtdPessoasAtual + qtdPessoas
                    && hotelResultado.qtdQuartos >= hotelResultado.qtdQuartosOcupados + qtdQuartos) {
                    // é possivel reservar hotel e passagem
                    // entao respondo com sucesso
                    funcSucesso(hotelResultado, passResultado);

                } else {
                    funcFalha({
                        "mensagem": "Nao foi possivel reservar o hotel"
                    });
                    return;
                }
            });

        } else {
            funcFalha({
                "mensagem": "Nao foi possivel reservar as passagens"
            });
            return;
        }
    });*/