"use strict"
var fs = require("fs");

class Transacao {
    constructor(dados, idTranscao) {
        this.votoHotel = false;
        this.votoPassagem = false;
        this.dados = dados;
        this.IdTrans = idTranscao;
        this.estado = ESTADO_ATIVA;
    }
}
const ESTADO_ATIVA = 1;
const ESTADO_EFETIVACAO_PROVISORIA = 2;
const ESTADO_FALHA = 3;
const ESTADO_EFETIVADA = 4;
const ESTADO_ABORTADA = 5;

var idTransAtual = JSON.parse(fs.readFileSync("arquivos/idCont.txt")).id;
console.log(idTransAtual);

class GerenciadorTransacoes {
    // colocar um logger aqui
    constructor(arquivo) {
        this.listaTransacoes = [];
        this.contIdTrans = idTransAtual;
        this.arquivo = arquivo;
    }

    criarTransacao(dados) {
        this.contIdTrans++;
        var a = new Transacao(dados, this.contIdTrans);
        this.listaTransacoes.push(a);
        this.salvarLog(a);
        fs.writeFileSync("arquivos/idCont.txt", JSON.stringify({ "id": this.contIdTrans }));
        return a;
    }
    salvarLog(transacao) {
        var dados = {
            "t": transacao,
            "data": new Date()
        }
        fs.appendFile(this.arquivo, JSON.stringify(dados) + "\n", (err) => {
            if (err) throw err;
            console.log("Salvo Transacao " + transacao.IdTrans + " " + transacao.estado + " " + this.arquivo);
        });
    }

    efetivarTransacao(IdTrans) {



    }
    adicionarTransacao(trans) {
        this.listaTransacoes.push(trans);
        this.salvarLog(trans);
    }
    mudarEstado(trans, estado) {
        trans.estado = estado;
        this.salvarLog(trans);
    }
    estadoEfetivacaoProvisoria(trans) {
        this.mudarEstado(trans, ESTADO_EFETIVACAO_PROVISORIA);
        this.salvarLog(trans);
    }
    estadoFalha(trans) {
        this.mudarEstado(trans, ESTADO_FALHA);
        this.salvarLog(trans);
    }
    estadoEfetiva(trans) {
        this.mudarEstado(trans, ESTADO_EFETIVADA);
        this.salvarLog(trans);
    }
    estadoAbortada(trans) {
        this.mudarEstado(trans, ESTADO_ABORTADA);
        this.salvarLog(trans);
    }


};

exports.Transacao = Transacao;
exports.GerenciadorTransacoes = GerenciadorTransacoes;