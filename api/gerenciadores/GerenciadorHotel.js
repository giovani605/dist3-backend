// esse vai gerenciar as operacoes com hoteis

"use strict"
// esse arquivo vai ser o pincipal para controlar ..
var bancoHotel = require("../bancos/bancoHotel");
var moduleGerenciador = require("./gerenciadorTransacoes");
var gerenciadorTransacoes = new moduleGerenciador.GerenciadorTransacoes("logs/logHotel.txt");

// Recupera todos os hoteis cadastrados
function buscarTodosHotel(callback) {

    // a funcao callback é chamada quando 
    // eu termino de recuperar os dados do banco
    bancoHotel.retornarTodosHoteis((resultado) => {
        callback(resultado);
    });

}
// busca o hotel pelo id
function buscarHotelId(id, callback) {
    bancoHotel.buscarHotelId(id, (resultado) => {
        callback(resultado);
    });
}

// data vem DD-MM
function processarData(a) {
    var vetor = a.split("-");
    var date = new Date();
    date.setDate(Number(vetor[0]));
    date.setMonth(Number(vetor[1]));
    return date;
}

// Reserva Hotel
function reservarHotel(dados, callback) {



    // Separa os dados do request
    var dataEntrada = processarData(dados.dataEntrada);
    var dataSaida = processarData(dados.dataSaida);
    var qtdQuartos = Number(dados.qtdQuartos);
    var qtdPessoas = Number(dados.qtdPessoas);
    var nome = dados.nome;

    console.log(dados.dataSaida + " " + dados.dataEntrada + " " + qtdPessoas + " " + qtdQuartos);
    // busca o hotel no banco
    bancoHotel.buscarHotelNome(nome, (resultado) => {
        if (!resultado) {
            callback({ "mensagem": "não encontrou o Hotel" });
            return;
        }
        // Verifica se tem espaço disponivel

        // como fazer?
        var total = bancoHotel.buscarHotelPeriodo(dataEntrada, dataSaida, resultado.reservas);

        if (resultado.qtdPessoas >= total["totalPessoas"] + qtdPessoas
            && resultado.qtdQuartos >= total["totalQuartos"] + qtdQuartos) {
            resultado.qtdPessoasAtual += qtdPessoas;
            resultado.qtdQuartosOcupados += qtdQuartos;
            // salva os dados de request no hotel
            resultado.reservas.push({
                "dataEntrada": dataEntrada,
                "dataSaida": dataSaida,
                "qtdQuartos": qtdQuartos,
                "qtdPessoas": qtdPessoas
            });
            bancoHotel.salvarDados();
            // responde com sucesso para o front end
            callback({
                "mensagem": "Hotel reservado com sucesso",
                "hotel": resultado
            });
        } else {
            callback({
                "mensagem": "Não foi possivel reservar o hotel",
                "hotel": resultado
            });
        }



    });
    // inserir logica ... 
}


// reserva a passagem para uma transacao
// responde com o voto sim ou nao
function reservarHotelTransacao(dados, callback) {

    // Separa os dados do request
    var dataEntrada = processarData(dados.dataEntrada);
    var dataSaida = processarData(dados.dataSaida);
    var qtdQuartos = Number(dados.qtdQuartos);
    var qtdPessoas = Number(dados.qtdPessoas);
    var nome = dados.nome;
    var idTrans = dados.transacao.IdTrans;
    var transacao = dados.transacao;
    var dadosTransacao = {
        "dataEntrada": dataEntrada,
        "dataSaida": dataSaida,
        "qtdPessoas": qtdPessoas,
        "qtdQuartos": qtdQuartos,
        "nome": dados.nome,
        "idTrans": dados.transacao.idTrans
    };

    transacao.dadosTransacao = dadosTransacao;
    gerenciadorTransacoes.adicionarTransacao(transacao);

    console.log("Reservar Hotel: " + dataEntrada + " " + dataSaida + " " + qtdPessoas + " " + qtdQuartos + " " + idTrans + " " + nome);
    // busca o hotel no banco final
    // implementar controle de concorrencia
    bancoHotel.buscarHotelNome(nome, (resultado) => {
        if (!resultado) {
            gerenciadorTransacoes.estadoFalha(transacao);
            callback({ "voto": false, "mensagem": "não encontrou o Hotel" });
            return;
        }
        // Verifica se tem espaço disponivel
        transacao.dadosTransacao.id = resultado.id;

        var total = bancoHotel.buscarHotelPeriodo(dataEntrada, dataSaida, resultado.reservas);

        if (resultado.qtdPessoas >= total["totalPessoas"] + qtdPessoas
            && resultado.qtdQuartos >= total["totalQuartos"] + qtdQuartos) {
            // salva os dados de request no hotel
            // salvar no meio temporario
            var temp = {
                "qtdPessoasFinal": total["totalPessoas"] + qtdPessoas,
                "qtdQuartosFinal": total["totalQuartos"] + qtdQuartos,
                "idTrans": dados.transacao.idTrans,
                "id": resultado.id,
                "dados": dadosTransacao,
                "transacao": transacao
            }
            bancoHotel.inserirTemporario(temp);


            gerenciadorTransacoes.estadoEfetivacaoProvisoria(transacao);

            // responde com sucesso para o front end
            callback({
                "voto": true,
                "mensagem": " Hotel Total Pessoas " + (total["totalPessoas"] + qtdPessoas) + " Quartos " + (total["totalQuartos"] + qtdQuartos),
                "hotel": resultado
            });
        } else {
            gerenciadorTransacoes.estadoFalha(transacao);
            callback({
                "voto": false,
                "mensagem": "Não foi possivel reservar o hotel excesso " +  (total["totalPessoas"] + qtdPessoas) + " Quartos " + (total["totalQuartos"] + qtdQuartos),
                "hotel": resultado
            });
        }



    });
    // inserir logica ... 
}
function efetivarTransacao(idTrans, callback) {
    console.log("inicando efetivacao TRansacao hotel " + idTrans);
    // buscar os dados no meio temporario
    bancoHotel.buscarDadoTemporario(idTrans, (resultado) => {
        // acha o dado no banco final 
        bancoHotel.buscarHotelId(resultado.id, (hotelFinal) => {
            // atualizar o fixo e salvar
            hotelFinal.qtdPessoasAtual = resultado.qtdPessoasFinal;
            hotelFinal.qtdQuartosOcupados = resultado.qtdQuartosFinal;
            hotelFinal.reservas.push(resultado.dados);
            bancoHotel.salvarDados();
            callback("sucesso");
            // bancoHotel.removerTemporario(resultado);
        });



    });

}


function abortarTransacao(idTrans, callback) {
    console.log("inicando Aborto TRans hotel " + idTrans);
    for (let a of gerenciadorTransacoes.listaTransacoes) {
        if (a["IdTrans"] == idTrans) {
            console.log("encontrou TRans hotel");

            gerenciadorTransacoes.estadoAbortada(a);
            callback("abortada com sucesso");
            return;
        }
    }

}

exports.abortarTransacao = abortarTransacao;
exports.reservarHotelTransacao = reservarHotelTransacao;
exports.efetivarTransacao = efetivarTransacao;

exports.reservarHotel = reservarHotel;
exports.buscarTodosHotel = buscarTodosHotel;
exports.buscarHotelId = buscarHotelId;


// for (let a of gerenciadorTransacoes.listaTransacoes) {
    //     if (a["IdTrans"] == idTrans) {
    //         console.log("encontrou TRans hotel");
    //         bancoHotel.buscarHotelId(a.dadosTransacao.id, (resultado) => {
    //             resultado.qtdPessoasAtual += a.dadosTransacao.qtdPessoas;
    //             resultado.qtdQuartosOcupados += a.dadosTransacao.qtdQuartos;
    //             bancoHotel.salvarDados();
    //             gerenciadorTransacoes.estadoEfetiva(a)
    //             callback("sucesso");
    //             return;
    //         })



    //     }
    // }