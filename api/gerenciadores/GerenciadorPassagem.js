// gerencia a partede passagens

"use strict"
// esse arquivo vai ser o pincipal para controlar ..

var bancoPassagem = require("../bancos/bancoPassagem");
var moduleGerenciador = require("./gerenciadorTransacoes");
var gerenciadorTransacoes = new moduleGerenciador.GerenciadorTransacoes("logs/logPass.txt");

// Recupera todas as passagens
function buscarTodasPassagem(callback) {

    // a funcao callback é chamada quando 
    // eu termino de recuperar os dados do banco
    bancoPassagem.retornarTodasPassagem((resultado) => {
        callback(resultado);
    });

}
// busca a passagem pelo id
function buscarPassId(id, callback) {
    bancoPassagem.buscarPassId(id, (resultado) => {
        callback(resultado);
    });
}
// Reserva a passagem
// data vem DD-MM
function processarData(a) {
    var vetor = a.split("-");
    var date = new Date();
    date.setDate(Number(vetor[0]));
    date.setMonth(Number(vetor[1]));
    return date;
}


function reservarPassagem(dados, callback) {
    // Separa os dados do request
    var dataIda = processarData(dados.dataIda);
    var dataVolta = processarData(dados.dataVolta);
    var qtdPessoas = Number(dados.qtdPessoas);
    var origem = dados.origem;
    var destino = dados.destino;
    var tipo = dados.tipo;

    console.log("Reservar passagem: " + dataIda + " " + dataVolta + " " + origem + " " + destino);
    // Busca a passagem correspondente
    

    bancoPassagem.buscarPassNome(origem, destino, (resultado) => {
        // se nao encontrar uma passagem retorna erro
        if (!resultado) {
            callback({ "mensagem": "não encontrou a Passagem" });
            return;
        }
        var total = bancoPassagem.buscarPassagemPeriodo(dataIda, resultado.reservas);
        // Verifica se ha espaço disponivel para os novos passageiros
        if (resultado.limitePassageiros >= total + qtdPessoas) {
            resultado.atualPassageiros += qtdPessoas;
            // salva os dados do request
            resultado.reservas.push({
                "dataIda": dataIda,
                "dataVolta": dataVolta,
                "qtdPessoas": qtdPessoas,
                "tipo": tipo
            });
            bancoPassagem.salvarDados();
            // retorna uma mensagem de sucesso para o cliente
            callback({
                "mensagem": "Passagens reservadas com sucesso",
                "pass": resultado
            });
        } else {
            callback({
                "mensagem": "Não foi possivel reservar as passagens",
                "hotel": resultado
            });
        }
    });


}

// reserva a passagem para uma transacao
// responde com o voto sim ou nao
function reservarPassagemTrans(dados, callback) {
    // Separa os dados do request
    var dataIda = processarData(dados.dataIda);
    var dataVolta = processarData(dados.dataVolta);
    var qtdPessoas = Number(dados.qtdPessoas);
    var origem = dados.origem;
    var destino = dados.destino;
    var tipo = dados.tipo;
    var idTrans = dados.transacao.idTrans;
    var dadosTransacao = {
        "dataIda": dataIda,
        "dataVolta": dataVolta,
        "qtdPessoas": qtdPessoas,
        "origem": dados.origem,
        "destino": dados.destino,
        "tipo": dados.tipo,
        "idTrans": dados.transacao.idTrans
    };

    var transacao = dados.transacao;
    transacao.dadosTransacao = dadosTransacao;
    gerenciadorTransacoes.adicionarTransacao(transacao);

    console.log("Reservar passagem: " + dataIda + " " + dataVolta + " " + origem + " " + destino + " " + idTrans);
    // Busca a passagem correspondente
    bancoPassagem.buscarPassNome(origem, destino, (resultado) => {
        // se nao encontrar uma passagem retorna erro
        if (!resultado) {
            this.gerenciadorTransacoes.estadoFalha(transacao);
            callback({ "voto": false, "mensagem": "não encontrou a Passagem" });
            return;
        }
        // salva nos dados da transacao o id do objeto encontrado
        transacao.dadosTransacao.id = resultado.id;


        var total = bancoPassagem.buscarPassagemPeriodo(dataIda, resultado.reservas);
        // Verifica se ha espaço disponivel para os novos passageiros
        if (resultado.limitePassageiros >= total + qtdPessoas) {
            // resultado.atualPassageiros += qtdPessoas;
            // salva os dados do request
            var temp = {
                "qtdPessoasFinal": resultado.atualPassageiros + qtdPessoas,
                "idTrans": dados.transacao.idTrans,
                "id": resultado.id,
                "transacao": transacao,
                "dados": dadosTransacao
            }
            bancoPassagem.inserirTemporario(temp);



            gerenciadorTransacoes.estadoEfetivacaoProvisoria(transacao);

            // // retorna uma mensagem de sucesso para o cliente
            callback({
                "voto": true,
                "mensagem": "Passagens podem serem reservas total : " + (total + qtdPessoas),
                "pass": resultado
            });
        } else {
            gerenciadorTransacoes.estadoFalha(transacao);
            callback({
                "voto": false,
                "mensagem": "Não foi possivel reservar as passagens excesso : " + (total + qtdPessoas),
                "hotel": resultado
            });
        }
    });


}

function efetivarTransacao(idTrans, callback) {
    console.log("inicando efetivacao TRansacao hotel " + idTrans);
    // buscar os dados no meio temporario
    bancoPassagem.buscarDadoTemporario(idTrans, (resultado) => {
        // acha o dado no banco final 
        bancoPassagem.buscarPassId(resultado.id, (passFinal) => {
            // atualizar o fixo e salvar
            passFinal.atualPassageiros = resultado.qtdPessoasFinal;
            passFinal.reservas.push(resultado.dados);
            bancoPassagem.salvarDados();
            callback("sucesso");
            // bancoHotel.removerTemporario(resultado);
        });



    });
}


function abortarTransacao(idTrans, callback) {
    console.log("inicando Aborto TRans hotel " + idTrans);
    for (let a of gerenciadorTransacoes.listaTransacoes) {
        if (a["IdTrans"] == idTrans) {
            console.log("encontrou TRans hotel");
            gerenciadorTransacoes.estadoAbortada(a);
            callback("abortada com sucesso");
            return;
        }
    }

}


exports.abortarTransacao = abortarTransacao;
exports.reservarPassagemTrans = reservarPassagemTrans;
exports.efetivarTransacao = efetivarTransacao;

exports.reservarPassagem = reservarPassagem;
exports.buscarTodasPassagem = buscarTodasPassagem;
exports.buscarPassId = buscarPassId;

// resultado.reservas.push({
            //     "dataIda": dataIda,
            //     "dataVolta": dataVolta,
            //     "qtdPessoas": qtdPessoas,
            //     "tipo": tipo
            // });