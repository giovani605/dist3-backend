"use strict"
var fs = require("fs");
var dadosMOCK = require("../model/dadosMOCK");
var dados = dadosMOCK.HOTEIS;
// Nao vai ser uma copia direta dos dados
// sera prencida conforme a execucao
var dadosTemporarios = dadosMOCK.HOTEISTEMP;
var tempo = 500;
//console.log(dadosTemporarios);

// Script que gerencia o "banco" de Hotel

function buscarHotelPeriodo(dataEntrada, dataSaida, reservas) {
    var totalQuartos = 0;
    var totalPessoas = 0;
    for (let a of reservas) {
        var dataInicial = new Date(a.dataEntrada);
        var dataFim = new Date(a.dataSaida);
        if ((dataInicial.getTime() >= dataEntrada.getTime() &&
            dataInicial.getTime() <= dataSaida.getTime()) ||
            (dataFim.getTime() >= dataEntrada.getTime() &&
                dataFim.getTime() <= dataSaida.getTime())) {
            totalQuartos += a.qtdQuartos;
            totalPessoas += a.qtdPessoas;
        }
    }
    return { "totalQuartos": totalQuartos, "totalPessoas": totalPessoas };
}
exports.buscarHotelPeriodo = buscarHotelPeriodo;

function retornarTodosHoteis(callback) {
    // salvarDados();
    // Apenas para simular uma consulta no banco
    setTimeout(() => {
        callback(dados);
    }, tempo);
}
function buscarHotelId(id, callback) {
    setTimeout(() => {
        for (var a of dados) {
            if (a.id == id) {
                callback(a);
                return;
            }
        }
        callback(null);
        return;
    }, tempo);
}
function buscarDadoTemporario(idTrans, callback) {
    setTimeout(() => {
        for (var a of dadosTemporarios) {
            if (a.transacao.IdTrans == idTrans) {
                callback(a);
                return;
            }
        }
        callback(null);
        return;
    }, tempo);
}
exports.buscarDadoTemporario = buscarDadoTemporario;


function buscarHotelNome(nome, callback) {
    setTimeout(() => {
        for (var a of dados) {
            if (a.nome == nome) {
                callback(a);
                return;
            }
        }
        callback(null);
        return;
    }, tempo);
}
// persiste os dados no final
function salvarDados() {
    fs.writeFile('arquivos/bancoHotel.txt', JSON.stringify(dados), (err) => {
        if (err) throw err;
        console.log("salvo Final");
    });
}
function salvarDadosTemp() {
    fs.writeFile('arquivos/bancoHotelTmp.txt', JSON.stringify(dadosTemporarios), (err) => {
        if (err) throw err;
        console.log("salvo Temp");
    });
}

function inserirTemporario(dados) {
    dadosTemporarios.push(dados);
    salvarDadosTemp();
}
exports.inserirTemporario = inserirTemporario;

// nao vai ser utilizado
function removerTemporario(dados) {
    index = dadosTemporarios.findIndex((d) => {
        console.log(d.IdTrans + " " + dados.IdTrans);
        return d.IdTrans == dados.IdTrans;
    });
    console.log(dadosTemporarios);
    salvarDadosTemp();
}

exports.removerTemporario = removerTemporario;
exports.salvarDados = salvarDados;
exports.salvarDadosTemp = salvarDadosTemp;
exports.retornarTodosHoteis = retornarTodosHoteis;
exports.buscarHotelId = buscarHotelId;
exports.buscarHotelNome = buscarHotelNome;