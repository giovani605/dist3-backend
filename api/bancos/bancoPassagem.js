"use strict"
var fs = require("fs");
var dadosMOCK = require("../model/dadosMOCK");
var dados = dadosMOCK.PASSAGEM;
var dadosTemporarios = dadosMOCK.PASSTEMP;
var tempo = 500;

function mesmodia(a, b) {
    var mesA = a.getMonth();
    var mesB = a.getMonth();
    var diaA = a.getDate();
    var diaB = a.getDate();
    if ((mesA == mesB) && (diaA == diaB)) {
        return true;
    }
    return false;

}


// Script que gerencia o "banco" de passagem
function buscarPassagemPeriodo(dataIda, reservas) {
    var totalPessoas = 0;
    for (let a of reservas) {
        var dataInicial = new Date(a.dataIda);
        if (mesmodia(dataIda, dataInicial)) {

            totalPessoas += a.qtdPessoas;
        }
    }
    return totalPessoas;
}
exports.buscarPassagemPeriodo = buscarPassagemPeriodo;


function retornarTodasPassagem(callback) {
    // Apenas para simular uma consulta no banco
    // salvarDados();
    setTimeout(() => {
        callback(dados);
    }, tempo);
}
function buscarPassId(id, callback) {
    setTimeout(() => {
        for (var a of dados) {
            if (a.id == id) {
                callback(a);
                return;
            }
        }
        callback({});
        return;
    }, tempo);


}
function buscarPassNome(origem, destino, callback) {
    setTimeout(() => {
        for (var a of dados) {
            if (a.origem == origem && a.destino == destino) {
                callback(a);
                return;
            }
        }
        callback({});
        return;
    }, tempo);
}
function salvarDados() {
    fs.writeFile('arquivos/bancoPass.txt', JSON.stringify(dados), (err) => {
        if (err) throw err;
        console.log("salvo");
    });
}
exports.salvarDados = salvarDados;

function buscarDadoTemporario(idTrans, callback) {
    setTimeout(() => {
        for (var a of dadosTemporarios) {
            if (a.transacao.IdTrans == idTrans) {
                callback(a);
                return;
            }
        }
        callback(null);
        return;
    }, tempo);
}
exports.buscarDadoTemporario = buscarDadoTemporario;


function salvarDadosTemp() {
    fs.writeFile('arquivos/bancoPassTmp.txt', JSON.stringify(dadosTemporarios), (err) => {
        if (err) throw err;
        console.log("salvo Temp");
    });
}

function inserirTemporario(dados) {
    dadosTemporarios.push(dados);
    salvarDadosTemp();
}
exports.inserirTemporario = inserirTemporario;
exports.salvarDadosTemp = salvarDadosTemp;



exports.retornarTodasPassagem = retornarTodasPassagem;
exports.buscarPassId = buscarPassId;
exports.buscarPassNome = buscarPassNome;