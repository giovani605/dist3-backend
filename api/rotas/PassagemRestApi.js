"use strict"

const express = require('express');
const router = express.Router();
var gerenciador = require("../gerenciadores/GerenciadorPassagem");
// Este arquivo cuida dos pedidos de passagem

// Recupera todas as passagens
router.get("/", (req, res, next) => {
    gerenciador.buscarTodasPassagem((dados) => {
        try {
            res.writeHead(200, { "Content-Type": "application/json" });
            res.end(JSON.stringify({
                "mensagem": "ok",
                "dados": dados
            }));
        }
        catch (e) {
            console.log("sadsad");
        }
    });
});

// Reservar a passagem
router.post("/reservar", (req, res, next) => {
    console.log(req.body);
    gerenciador.reservarPassagem(req.body,(resultado) => {
        try {
            // escreve a resposta
            res.writeHead(200, { "Content-Type": "application/json" });
            res.end(JSON.stringify({
                "mensagem": "ok",
                "dados": resultado
            }));
        }
        catch (e) {
            console.log("sadsad");
        }
    });
});


// Reservar a passagem TRansacao
router.post("/reservar/transacao", (req, res, next) => {
    console.log(req.body);
    gerenciador.reservarPassagemTrans(req.body,(resultado) => {
        try {
            // escreve a resposta
            res.writeHead(200, { "Content-Type": "application/json" });
            res.end(JSON.stringify({
                "mensagem": "ok",
                "dados": resultado
            }));
        }
        catch (e) {
            console.log("sadsad");
        }
    });
});

// Reservar a passagem TRansacao
router.post("/efetivar/transacao/:id", (req, res, next) => {
    console.log(req.body);
    gerenciador.efetivarTransacao(req.params.id,(resultado) => {
        try {
            // escreve a resposta
            res.writeHead(200, { "Content-Type": "application/json" });
            res.end(JSON.stringify({
                "mensagem": "ok",
                "dados": resultado
            }));
        }
        catch (e) {
            console.log("sadsad");
        }
    });
});

// Reservar a passagem TRansacao
router.post("/abortar/transacao/:id", (req, res, next) => {
    gerenciador.abortarTransacao(req.params.id,(resultado) => {
        try {
            // escreve a resposta
            res.writeHead(200, { "Content-Type": "application/json" });
            res.end(JSON.stringify({
                "mensagem": "ok",
                "dados": resultado
            }));
        }
        catch (e) {
            console.log("sadsad");
        }
    });
});

// Recupera passagem pelo ID
router.get("/:id", (req, res, next) => {
    var id = req.params.id;
    gerenciador.buscarPassId(id, (dados) => {
        // Responde ao cliente
        res.status(200).send({
            "mensagem": "ok",
            "dados": dados
        });
    });
});




module.exports = router;