"use strict"

const express = require('express');
const router = express.Router();
var gerenciador = require("../gerenciadores/GerenciadorHotel");

// Este arquivo cuida dos pedidos de hospedagem


// Recupera todas as passagens
router.get("/", (req, res, next) => {
    gerenciador.buscarTodosHotel((dados) => {
        try {
            // Responde ao cliente
            res.writeHead(200, { "Content-Type": "application/json" });
            res.end(JSON.stringify({
                "mensagem": "ok",
                "dados": dados
            }));
        }
        catch (e) {
            console.log("problemas");
        }
    });
});
// Reserva o Hotel
router.post("/reservar", (req, res, next) => {
    gerenciador.reservarHotel(req.body,(resultado) => {
        try {
            // Responde ao cliente
            res.writeHead(200, { "Content-Type": "application/json" });
            res.end(JSON.stringify({
                "mensagem": "ok",
                "dados": resultado
            }));
        }
        catch (e) {
            console.log("problemas");
        }
    });
});

// Reserva o Hotel transacao
router.post("/reservar/transacao", (req, res, next) => {
    gerenciador.reservarHotelTransacao(req.body,(resultado) => {
        try {
            // Responde ao cliente
            res.writeHead(200, { "Content-Type": "application/json" });
            res.end(JSON.stringify({
                "mensagem": "ok",
                "dados": resultado
            }));
        }
        catch (e) {
            console.log("problemas");
        }
    });
});


// Reservar a passagem TRansacao
router.post("/efetivar/transacao/:id", (req, res, next) => {
    gerenciador.efetivarTransacao(req.params.id,(resultado) => {
        try {
            // escreve a resposta
            res.writeHead(200, { "Content-Type": "application/json" });
            res.end(JSON.stringify({
                "mensagem": "ok",
                "dados": resultado
            }));
        }
        catch (e) {
            console.log("sadsad");
        }
    });
});

// Reservar a passagem TRansacao
router.post("/abortar/transacao/:id", (req, res, next) => {
    gerenciador.abortarTransacao(req.params.id,(resultado) => {
        try {
            // escreve a resposta
            res.writeHead(200, { "Content-Type": "application/json" });
            res.end(JSON.stringify({
                "mensagem": "ok",
                "dados": resultado
            }));
        }
        catch (e) {
            console.log("sadsad");
        }
    });
});


// Recupera o hotel pelo Id
router.get("/:id", (req, res, next) => {
    var id = req.params.id;
    gerenciador.buscarHotelId(id, (dados) => {
        // Responde ao cliente
        res.status(200).send({
            "mensagem": "ok",
            "dados": dados
        });
    });
});





module.exports = router;