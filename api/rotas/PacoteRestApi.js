"use strict"

const express = require('express');
const router = express.Router();
var gerenciador = require("../model/GerenciadorPrincipal");
// Este arquivo cuida dos pedidos das Pacote

// Recupera todas as passagens e goteis
router.get("/", (req, res, next) => {
    gerenciador.buscarTodosHotel((hoteis) => {
        gerenciador.buscarTodasPassagem((passagens) => {
            try {
                // Responde ao cliente
                res.writeHead(200, { "Content-Type": "application/json" });
                res.end(JSON.stringify({
                    "mensagem": "ok",
                    "hotel": hoteis,
                    "passagem" : passagens
                }));
            }
            catch (e) {
                console.log("problemas");
            }
        })
    });
});
// Reservar a passagem
router.post("/reservar", (req, res, next) => {
    gerenciador.reservarPacote(req.body, (resultado) => {
        try {
            res.writeHead(200, { "Content-Type": "application/json" });
            res.end(JSON.stringify({
                "mensagem": "ok",
                "dados": resultado
            }));
        }
        catch (e) {
            console.log("problemas");
        }
    });
});






module.exports = router;