"use strict"

const express = require('express');
const router = express.Router();
var gerenciador = require("../gerenciadores/GerenciadorCoordenador");
// Este arquivo cuida dos pedidos das Pacote

// Recupera todas as passagens e hoteis
router.get("/todos", (req, res, next) => {
    gerenciador.buscarTodosHotel((hoteis) => {
        gerenciador.buscarTodasPassagem((passagens) => {
            try {
                // Responde ao cliente
                res.writeHead(200, { "Content-Type": "application/json" });
                res.end(JSON.stringify({
                    "mensagem": "ok",
                    "hotel": hoteis,
                    "passagem" : passagens
                }));
            }
            catch (e) {
                console.log("problemas");
            }
        })
    });
});

// Reservar a passagem
router.post("/passagem/reservar", (req, res, next) => {
    gerenciador.reservarPassagem(req.body, (resultado) => {
        try {
            res.writeHead(200, { "Content-Type": "application/json" });
            res.end(JSON.stringify({
                "mensagem": "ok",
                "dados": resultado
            }));
        }
        catch (e) {
            console.log("problemas");
        }
   
   
    });
});

// Reserva o Hotel
router.post("/hotel/reservar", (req, res, next) => {
    gerenciador.reservarHotel(req.body,(resultado) => {
        try {
            // Responde ao cliente
            res.writeHead(200, { "Content-Type": "application/json" });
            res.end(JSON.stringify({
                "mensagem": "ok",
                "dados": resultado
            }));
        }
        catch (e) {
            console.log("problemas");
        }
    });
});

// Recupera todas as passagens
router.get("/passagem/", (req, res, next) => {
    gerenciador.buscarTodasPassagem((dados) => {
        try {
            // Responde ao cliente
            res.writeHead(200, { "Content-Type": "application/json" });
            res.end(JSON.stringify({
                "mensagem": "ok",
                "resposta" : dados
            }));
        }
        catch (e) {
            console.log("problemas");
        }
    });
});

// Recupera todas as hotel
router.get("/hotel/", (req, res, next) => {
    gerenciador.buscarTodosHotel((dados) => {
        try {
            // Responde ao cliente
            res.writeHead(200, { "Content-Type": "application/json" });
            res.end(JSON.stringify({
                "mensagem": "ok",
                "resposta" : dados
            }));
        }
        catch (e) {
            console.log("problemas");
        }
    });
});



// Recupera o hotel pelo Id
router.get("/hotel/:id", (req, res, next) => {
    var id = req.params.id;
    gerenciador.buscarHotelId(id, (dados) => {
        // Responde ao cliente
        res.status(200).send({
            "mensagem": "ok",
            "dados": dados
        });
    });
});

// Reservar a pacote
router.post("/pacote/reservar", (req, res, next) => {
    gerenciador.reservarPacote(req.body, (resultado) => {
        try {
            res.writeHead(200, { "Content-Type": "application/json" });
            res.end(JSON.stringify({
                "mensagem": "ok",
                "dados": resultado
            }));
        }
        catch (e) {
            console.log("problemas");
        }
    });
});




module.exports = router;