const app = require("./app");

const appHotel = require("./appHotel");
const appPass = require("./appPassagem");



const http = require("http");

// cria um servidor de http com a middleware express
const server = http.createServer(app);

// Coloca o servidor para escutar na porta 3001
server.listen(3001,function(){
    console.log("Servidor Coordenador  rodando");
});


// const serverHotel = http.createServer(appHotel);
// // Coloca o servidor para escutar na porta 3001
// serverHotel.listen(4000,function(){
//     console.log("Servidor Hotel rodando");
// });



// const serverPass = http.createServer(appPass);

// // Coloca o servidor para escutar na porta 3001
// serverPass.listen(4001,function(){
//     console.log("Servidor Passagem rodando");
// });
