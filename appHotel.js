var express = require('express');
var app = express();
var bodyParser = require('body-parser');
var HotelRest = require("./api/rotas/HotelRestApi");


// Este Script define a middleware

// A middleware utiliza um parser de Json
app.use(bodyParser.json());


// Configuração de Headers para evitar conflitos de Cross Origin
app.use((req, res, next) => {
    
    res.setHeader("Access-Control-Allow-Origin", "*");
    res.setHeader(
        "Access-Control-Allow-Headers",
        "Origin, X-Requested-With, Content-Type, Accept, Authorization"
    );
    res.setHeader(
        "Access-Control-Allow-Methods",
        "GET, POST, PATCH, PUT, DELETE, OPTIONS"
    );
    next();
});

// Aqui defino as rotas e quem é responsavel por lidar pelos requests
app.use("/hotel",HotelRest);




module.exports = app;